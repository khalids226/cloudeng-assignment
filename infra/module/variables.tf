# Global varibales
# Service name definition
variable "service_name" {
  default = "DummyCluster"
}

# Maps of short workspace names for Terraform naming
variable "short_ws_names" {
  type = "string"
}

# VPC variables
variable "vpc_id" {}

variable "old_workspace" {
  type = "string"
}

variable "private_subnets" {
  type = "list"
}

# ECS variables
variable "instance_type" {
  default = "m4.large"
}

variable "key_name" {
  default = "DummyCluster"
}

variable "cluster_min_size" {
  default = "2"
}

variable "cluster_max_size" {
  default = "2"
}

variable "cluster_name" {}

variable "sns_topic_arn" {}

variable "enable" {}

# if/else alternative

