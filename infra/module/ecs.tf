resource "aws_ecs_cluster" "cluster" {
  name = "${var.cluster_name}"
}

resource "aws_ecs_service" "service" {
  name          = "${var.name}"
  cluster       = "${aws_ecs_cluster.cluster.arn}"
  launch_type   = "FARGATE"
  desired_count = "${var.desired_count}"

  network_configuration {
    security_groups = ["${aws_security_group.ecs.id}"]
    subnets         = ["${data.aws_subnet_ids.private.ids}"]
  }

  task_definition = "${aws_ecs_task_definition.this.arn}"

  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 75

  load_balancer {
    target_group_arn = "${aws_alb_target_group.this.arn}"
    container_name   = "${var.name}"
    container_port   = "${var.port}"
  }

  depends_on = ["aws_alb_listener.https"]
}

data "aws_subnet_ids" "private" {
  vpc_id = "${data.aws_vpc.stage.id}"

  tags {
    Type = "private"
  }
}

resource "aws_security_group" "ecs" {
  name        = "${var.name}-ecs"
  description = "${var.name} ecs"
  vpc_id      = "${data.aws_vpc.stage.id}"

  ingress {
    from_port       = "${var.port}"
    to_port         = "${var.port}"
    protocol        = "${var.protocol}"
    security_groups = ["${aws_security_group.lb.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.tags,map("Name","${var.name}-ecs"))}"
}

data "template_file" "container_definition" {
  template = "${file("${path.module}/container_definition.tpl")}"

  vars = {
    command        = "${jsonencode(var.command)}"
    ecr_repo_name  = "${coalesce(var.ecr_repo_name, var.name)}"
    docker_tag     = "${var.docker_tag}"
    name           = "${var.name}"
    environment    = "${jsonencode(var.environment)}"
    secrets        = "${jsonencode(var.secrets)}"
    cpu            = "${var.cpu}"
    memory         = "${var.memory}"
    port           = "${var.port}"
    protocol       = "${var.protocol}"
    log_group_name = "${aws_cloudwatch_log_group.this.name}"
    region         = "${data.aws_region.current.name}"
  }
}

resource "aws_ecs_task_definition" "this" {
  family                   = "${var.name}"
  container_definitions    = "${data.template_file.container_definition.rendered}"
  task_role_arn            = "${aws_iam_role.task.arn}"
  execution_role_arn       = "${aws_iam_role.execution.arn}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "${var.cpu}"
  memory                   = "${var.memory}"
  tags                     = "${var.tags}"

  depends_on = ["aws_cloudwatch_log_group.this"]
}

resource "aws_appautoscaling_target" "this" {
  max_capacity       = "${var.scaling["max_capacity"]}"
  min_capacity       = "${var.scaling["min_capacity"]}"
  resource_id        = "service/${data.aws_ecs_cluster.cluster.cluster_name}/${aws_ecs_service.this.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "this" {
  name               = "${var.scaling["metric"]}-scaling-policy"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${data.aws_ecs_cluster.cluster.cluster_name}/${aws_ecs_service.this.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "${var.scaling["metric"] == "memory" ? "ECSServiceAverageMemoryUtilization" : "ECSServiceAverageCPUUtilization"}"
    }

    scale_in_cooldown = "${lookup(var.scaling, "scale_in_cooldown", 0)}"
    scale_out_cooldown = "${lookup(var.scaling, "scale_out_cooldown", 0)}"
    target_value = "${var.scaling["threshold"]}"
  }

  depends_on = ["aws_appautoscaling_target.this"]
}
